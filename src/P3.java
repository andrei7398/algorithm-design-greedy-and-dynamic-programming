import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class P3 {
	
	static class Pair{
        long player1, player2;
        int pick = 0;
    }

    public static void main (String[] args) throws FileNotFoundException {
    	
		Scanner scan = new Scanner(new File("p3.in"));
		
		// Citire numere 
		int n;
		n = scan.nextInt();
		int[] numbers = new int[n];
		for (int i = 0; i < n; i++) {
			numbers[i] = scan.nextInt();
		}
		scan.close();
		
		// Salvam de fiecare data scorurile intr-o pereche
        Pair[][] moves = new Pair[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < moves[i].length; j++) {
                moves[i][j] = new Pair();
            }
        }
        
        for(int i = 0; i < n; i++) {
            moves[i][i].player1 = numbers[i];
            // Secventa de mutari
            moves[i][i].pick = i;
        }
        
        for (int l = 2; l <= n; l++) {
            for (int i = 0; i <= n - l; i++) {
                int j = i + l - 1;
                // Verific scorul oponentului pe subvectorul din dreapta  
                if (numbers[i] + moves[i+1][j].player2 > moves[i][j-1].player2 + numbers[j]) {
                    moves[i][j].player1 = numbers[i] + moves[i+1][j].player2;
                    moves[i][j].player2 = moves[i+1][j].player1;
                    moves[i][j].pick = i;
                }
                // Verific scorul oponentului pe subvectorul din stanga   
                else {
                    moves[i][j].player1 = numbers[j] + moves[i][j-1].player2;
                    moves[i][j].player2 = moves[i][j-1].player1;
                    moves[i][j].pick =j;
                }
            }
        }

        // Deschid printerul
		PrintWriter writer = new PrintWriter("p3.out");
		
		// Scriu rezultatul final
    	writer.printf("%d", moves[0][n - 1].player1 - moves[0][n - 1].player2);
    	
	    // Inchid printerul
	    writer.close();
    }
}
