import java.util.*;
import java.io.*;

public class P1 {
	public static void main(String[] args) throws FileNotFoundException, IOException {
		
		Scanner scan = new Scanner(new File("p1.in"));
		
		int n;
		ArrayList<Integer> numbers = new ArrayList<Integer>();
		
		// Citire numere
		n = scan.nextInt();
		for (int i = 0; i < n; i++) {
			numbers.add(scan.nextInt());
		}
		
		scan.close();
		
		// Sortez vectorul
		Collections.sort(numbers);
		
		// Scorurile celor 2 jucatori
		int player1 = 0, player2 = 0;
		
		// 
		while (numbers.size() > 0) {
			player1 += numbers.get(numbers.size() - 1);
			numbers.remove(numbers.size() - 1);
			// Posibilitate numar impar de numere 
			// -> primul mai alege odata ultima oara
			if (numbers.size() > 0) {
				player2 += numbers.get(numbers.size() - 1);
				numbers.remove(numbers.size() - 1);
			}
		}
		
		// Deschid printerul
		PrintWriter writer = new PrintWriter("p1.out", "UTF-8");
		
		// Scriu rezultatul final
	    writer.printf("%d", player1-player2);
	    
	    // Inchid printerul
	    writer.close();
	}
}
